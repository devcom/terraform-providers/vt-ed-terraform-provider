package vted

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"net/url"
)

func dataSourceVtEdGroups() *schema.Resource {
	return &schema.Resource{
		Read: dataSourceVtEdGroupsRead,
		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Description: "The name/uugid to search for. Supports wildcards with *",
				Computed:    true,
				Optional:    true,
				AtLeastOneOf: []string{
					"created_before",
					"created_after",
					"expires_before",
					"expires_after",
					"contains_admin",
					"contains_contact",
					"contains_manager",
					"contains_member",
					"contains_viewer",
				},
			},
			"created_before": {
				Type:        schema.TypeString,
				Description: "Filter to find groups created prior to given date",
				Optional:    true,
				Computed:    false,
			},
			"created_after": {
				Type:        schema.TypeString,
				Description: "Filter to find groups created after given date",
				Optional:    true,
				Computed:    false,
			},
			"expires_before": {
				Type:        schema.TypeString,
				Description: "Filter to find groups that expire prior to given date",
				Optional:    true,
				Computed:    false,
			},
			"expires_after": {
				Type:        schema.TypeString,
				Description: "Filter to find groups that expire after given date",
				Optional:    true,
				Computed:    false,
			},
			"contains_admin": {
				Type:        schema.TypeString,
				Description: "Filter to find groups that contain an admin with the given uupid/uusid",
				Optional:    true,
				Computed:    false,
			},
			"contains_contact": {
				Type:        schema.TypeString,
				Description: "Filter to find groups that contain a contact with the given uupid/uusid",
				Optional:    true,
				Computed:    false,
			},
			"contains_manager": {
				Type:        schema.TypeString,
				Description: "Filter to find groups that contain a manager with the given uupid/uusid",
				Optional:    true,
				Computed:    false,
			},
			"contains_member": {
				Type:        schema.TypeString,
				Description: "Filter to find groups that contain a member with the given uupid/uusid",
				Optional:    true,
				Computed:    false,
			},
			"contains_viewer": {
				Type:        schema.TypeString,
				Description: "Filter to find groups that contain a service viewer with the given uusid",
				Optional:    true,
				Computed:    false,
			},
			"groups": {
				Type:        schema.TypeList,
				Description: "The result groups",
				Computed:    true,
				Elem:        GroupSummary(),
			},
			"names": {
				Type:        schema.TypeSet,
				Description: "The names of the groups found in the search",
				Computed:    true,
				Elem:        schema.TypeString,
				Set:         schema.HashString,
			},
		},
	}
}

func dataSourceVtEdGroupsRead(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	edClient := providerConfig.client

	queryParams := getGroupSearchQueryParams(d)
	data, err := edClient.Get("/groups?" + queryParams)
	if err != nil {
		return err
	}

	var names []interface{}
	var groups []map[string]interface{}
	for _, group := range data.Array() {
		g := map[string]interface{}{
			"uugid":           group.Get("uugid").String(),
			"name":            group.Get("uugid").String(),
			"display_name":    group.Get("displayName").String(),
			"creation_date":   group.Get("creationDate").String(),
			"expiration_date": group.Get("expirationDate").String(),
		}
		groups = append(groups, g)
		names = append(names, group.Get("uugid").String())
	}

	d.Set("groups", groups)
	d.Set("names", schema.NewSet(schema.HashString, names))
	d.SetId(queryParams)

	return nil
}

func getGroupSearchQueryParams(d *schema.ResourceData) string {
	uugid, uugidOk := d.GetOk("name")
	createdBefore, createdBeforeOk := d.GetOk("created_before")
	createdAfter, createdAfterOk := d.GetOk("created_after")
	expiresBefore, expiresBeforeOk := d.GetOk("expires_before")
	expiresAfter, expiresAfterOk := d.GetOk("expires_after")
	containsAdmin, containsAdminOk := d.GetOk("contains_admin")
	containsContact, containsContactOk := d.GetOk("contains_contact")
	containsManager, containsManagerOk := d.GetOk("contains_manager")
	containsMember, containsMemberOk := d.GetOk("contains_member")
	containsViewer, containsViewerOk := d.GetOk("contains_viewer")

	params := url.Values{}
	if uugidOk {
		params.Add("uugid", uugid.(string))
	}

	if createdBeforeOk {
		params.Add("crbefore", createdBefore.(string))
	}

	if createdAfterOk {
		params.Add("crafter", createdAfter.(string))
	}

	if expiresBeforeOk {
		params.Add("exbefore", expiresBefore.(string))
	}

	if expiresAfterOk {
		params.Add("exafter", expiresAfter.(string))
	}

	if containsAdminOk {
		params.Add("administrator", containsAdmin.(string))
	}

	if containsContactOk {
		params.Add("contact", containsContact.(string))
	}

	if containsManagerOk {
		params.Add("manager", containsManager.(string))
	}

	if containsMemberOk {
		params.Add("member", containsMember.(string))
	}

	if containsViewerOk {
		params.Add("viewer", containsViewer.(string))
	}

	return params.Encode()
}
