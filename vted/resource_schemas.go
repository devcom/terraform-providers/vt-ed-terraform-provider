package vted

import "github.com/hashicorp/terraform-plugin-sdk/helper/schema"

func PersonResource() *schema.Resource {
	return &schema.Resource{
		Schema: map[string]*schema.Schema{
			"display_name": {
				Type:        schema.TypeString,
				Description: "The display name of the person",
				Computed:    true,
			},
			"pid": {
				Type:        schema.TypeString,
				Description: "The pid of the person",
				Computed:    true,
			},
			"uid": {
				Type:        schema.TypeInt,
				Description: "The uid of the person",
				Computed:    true,
			},
		},
	}
}

func GroupSummary() *schema.Resource {
	return &schema.Resource{
		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Description: "The name/uugid of the group",
				Computed:    true,
			},
			"uugid": {
				Type:        schema.TypeString,
				Description: "The name/uugid of the group",
				Computed:    true,
			},
			"display_name": {
				Type:        schema.TypeString,
				Description: "The display name of the group",
				Computed:    true,
			},
			"creation_date": {
				Type:        schema.TypeString,
				Description: "The creation date of the group",
				Computed:    true,
			},
			"expiration_date": {
				Type:        schema.TypeString,
				Description: "The expiration date of the group",
				Computed:    true,
			},
		},
	}
}

type EdPerson struct {
	creationDate string
	displayName  string
	kind         string
	pid          string
	uid          int
}
