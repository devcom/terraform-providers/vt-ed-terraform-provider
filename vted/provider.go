package vted

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
)

type ProviderConfig struct {
	client    *EdClient
	serviceId string
}

func Provider() terraform.ResourceProvider {
	p := &schema.Provider{

		Schema: map[string]*schema.Schema{
			"base_url": {
				Type:        schema.TypeString,
				Optional:    true,
				Default:     "https://apps.middleware.vt.edu/ws/v1",
				Description: "Base url for API requests",
			},
			"service_auth": {
				Type:        schema.TypeMap,
				Description: "Auth details to connect to the ED",
				Required:    true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"key_cert_path": {
							Type:        schema.TypeString,
							Optional:    false,
							Description: "Path to service's certificate (PEM-encoded)",
						},
						"private_key_path": {
							Type:        schema.TypeString,
							Optional:    false,
							Description: "Path to service private key (PEM-encoded)",
						},
						"ca_cert_path": {
							Type:        schema.TypeString,
							Optional:    false,
							Description: "Path to the ca cert (PEM-encoded)",
						},
					},
				},
			},
		},

		DataSourcesMap: map[string]*schema.Resource{
			"vted_group":  dataSourceVtEdGroup(),
			"vted_groups": dataSourceVtEdGroups(),
		},

		ResourcesMap: map[string]*schema.Resource{
			"vted_group": resourceVtEdGroup(),
		},

		ConfigureFunc: func(d *schema.ResourceData) (interface{}, error) {
			serviceAuthConfig := d.Get("service_auth").(map[string]interface{})

			client := NewEdClient(
				d.Get("base_url").(string),
				serviceAuthConfig["ca_cert_path"].(string),
				serviceAuthConfig["private_key_path"].(string),
				serviceAuthConfig["key_cert_path"].(string),
			)

			response, err := client.Get("/whoami")
			if err != nil {
				return nil, err
			}

			providerConfig := new(ProviderConfig)
			providerConfig.serviceId = response.Get("uusid").Str
			providerConfig.client = client

			return providerConfig, nil
		},
	}

	return p
}
