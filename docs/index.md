
# VT ED Provider

The VT ED provider is used to interact with the Virginia Tech ED REST API. [Additional documentation about the VT ED webservices can be found here.](https://middleware.vt.edu/ed/ws/operations.html)

Authentication is required to connect to the ED services. As of right now, this provider only supports using a service credential.

## Example Usage

```hcl
# Configure the VTED provider
provider "vted" {
  service_auth = {
    private_key_path = "path/to/client.key"
    key_cert_path    = "path/to/client.crt"
    ca_cert_path     = "path/toca.crt"
  }
}

resource "vted_group" "example" {
  name            = "example.group"
  expiration_date = "2020-10-10"

  admins   = ["pid1", "pid2"]
  contacts = ["pidA", "pidB"]

  members {
    people = ["pid1", "pid2"]
    groups = ["example.another-group"]
  }
}
``` 

## Argument Reference

The following arguments are supported in the `provider` block:

- `service_auth` (Required) - A [service auth](#service-auth-arguments) block that configures mutual TLS authentication

### Service Auth Arguments

All files for service authentication should be PEM-encoded. A passphrase for the private key is currently not supported.

- `private_key_path` (Required) - The file path to the service's private key
- `key_cert_path` (Required) - The file path to the service's certificate
- `ca_cert_path` (Required) - The file path to a CA that should be used to verify the server's certificate
