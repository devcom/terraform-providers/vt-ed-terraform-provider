
# vted_group

This resource allows you to create and manage an ED group.

## Example usage

```hcl
resource "vted_group" "my_group" {
  name             = "it.platform.test1"
  expiration_date  = "2020-10-10"
  suppress_display = true
  suppress_members = true
  
  admins {
    people   = ["mikesir"]
    services = ["example"]
  }
  contacts = ["mikesir", "ceharris"]
  managers {
    people   = ["mikesir", "ceharris"]
    services = ["example"]
  }
  
  members {
    people = ["mikesir", "ceharris"]
    groups = ["it.platform.roles.role1"]
  }
  
  viewers = ["gateway"]
}
```

## Argument Reference

The following arguments are supported:

- `name` - (Required) The name of the group.
- `expiration_date` - (Optional) The expiration date of the group. Supported formats are RFC3339 and `YYYY-MM-DD`. Note that when using `YYYY-MM-DD`, the time is set to 00:00 UTC, which might cause date display differences.
- `admins` - (Required) A map of the `people` (PIDs) and `services` (uusids) that are allowed to administer the group. **Note**: The TF provider service account is always a service admin for the group.
- `contacts` - (Required) A collection of PIDs that are contacts for the group.
- `managers` - (Optional) A map of the `people` (PIDs) and `services` (uusids) that are allowed to manage the group membership.
- `members` - (Optional) A map of the `people` (PIDs) and `groups` (uusids) that are members of this group
- `viewers` - (Optional) An array of the `services` (uusids) that are viewers of the group
- `suppress_display` - (Optional) Boolean, defaults to true. Is this group visible in public listings?
- `suppress_members` - (Optional) Boolean, defaults to true. Is this group's membership visible in public queries?
- `replication_targets` - (Optional) A collection of downstream replication targets. Defaults to empty. Available targets: AD, GIT, GOOG

## Attributes Reference

The resource exports all argument attributes, as well as the following:

- `creation_date` - The date in which the group was created


## Importing groups

You can import a group using the `terraform import <resource> <id>` command, using the group's name as it's `id` value. Example:

```bash
terraform import vted_group.example my.example.group.id
```
