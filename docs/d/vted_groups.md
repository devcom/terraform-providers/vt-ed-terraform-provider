# vted_groups

Provides the ability to lookup many groups.

## Example Usage

```hcl
data "vted_groups" "app_roles" {
  name = "example.app.roles.*"
}
```

## Argument Reference

The following arguments are supported. At least one is required:

- `name` (Optional) - The name/uugid of the group to search for. Supports wildcards using `*`
- `created_before` (Optional) - Filter for groups created before the provided RFC3339 date
- `created_after` (Optional) - Filter for groups created after the provided RFC3339 date
- `expires_before` (Optional) - Filter for groups that expire before the provided RFC3339 date
- `expires_after` (Optional) - Filter for groups that expire after the provided RFC3339 date
- `contains_admin` (Optional) - Filter for groups that have the provided PID as an admin
- `contains_contact` (Optional) - Filter for groups that have the provided PID as a contact
- `contains_manager` (Optional) - Filter for groups that have the provided PID as a manager
- `contains_member` (Optional) - Filter for groups that have the provided PID or group name as a member
- `contains_viewer` (Optional) - Filter for groups that have the provided service id as an admin

## Attributes Reference

The resource exports the following attributes:

- `names` - A set of the names found in the search
- `groups` - A list of the [group results](#group-result-attributes)

### Group Result Attributes

Each group result has the following attributes:

- `name` - The group's name (same as `uugid`)
- `uugid` - The group's uugid (also exported as `name`)
- `display_name` - The display name of the group. Empty string if not set
- `creation_date` - The group's expiration date as a RFC3339 date
- `expiration_date` - The group's expiration date as a RFC3339 date
