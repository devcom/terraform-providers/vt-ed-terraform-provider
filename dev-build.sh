#!/bin/bash
# Build and deploy to your local workstation
# (assumes your running 64bit linux)
set -ex

docker build -t vt-ed-terraform-provider .

CONTAINER_ID=$(docker create vt-ed-terraform-provider)
docker cp $CONTAINER_ID:/ ./build
docker rm $CONTAINER_ID
[ -f ~/.terraform.d/plugins/terraform.vt.edu/vt/ed/1.0.0/linux_amd64/ ] || mkdir -p ~/.terraform.d/plugins/terraform.vt.edu/vt/ed/1.0.0/linux_amd64/
mv build/terraform-provider-vted-linux-amd64 ~/.terraform.d/plugins/terraform.vt.edu/vt/ed/1.0.0/linux_amd64/terraform-provider-ed 
