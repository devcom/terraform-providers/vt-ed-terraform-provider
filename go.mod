module github.com/terraform-provider-vt-ed-group

go 1.14

require (
	github.com/hashicorp/terraform-plugin-sdk v1.13.1
	github.com/tidwall/gjson v1.6.0
)
